import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import LoginContextProvider from './context/LoginContext';
import NavigationLogin from './navigation/navigationLogin';

const Stack = createNativeStackNavigator();

const Root = () => {
  return (
    <LoginContextProvider>
      <NavigationContainer>
        <NavigationLogin>
        </NavigationLogin>
      </NavigationContainer>
    </LoginContextProvider>
  );
};


export default Root;

