import React, {useContext, useState}from 'react'
import { View, Text, Pressable, StyleSheet, useWindowDimensions, Image } from 'react-native'

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
        flexDirection:'column',
    },
})

const openingpage = ({navigation}) => {
    const windowHeight = useWindowDimensions().height
    const windowWidth = useWindowDimensions().width

    const changeLogin = ()=>{
        navigation.navigate("login")
    }
   
    return (
        <View style={{height:windowHeight}}>
            <View style={styles.container}>
                <View style={{height:450, borderRadius:30, margin:20,backgroundColor:'aliceblue'}}>
                    <Pressable style={{width:40, height:40, borderRadius:20, backgroundColor:'#0072BB', margin:5,position:'absolute', right:10}}>
                        <Text style={{alignSelf:'center', padding:10, color:'white'}}>EN</Text>
                    </Pressable>

                    <Image style={{height:350, width:350, margin:20}} source={require('../image/login-wallpaper.png')}/>
                </View>

                <Text style={{fontSize:45,padding:10, color:'black',textAlign:'center', fontWeight:'bold'}}>Make Your Ideas Come Alive</Text>
                <Text style={{fontSize:20,margin:20, color:'black',textAlign:'center'}}>Join us to build your awesome idea, there will be more teams you can meet</Text>
                
                <View style={{flexDirection:'row', justifyContent:"space-between", margin:50, bottom:30}}>
                    <Pressable style={{height:50,width:150,borderRadius:5,backgroundColor:'white',justifyContent:'center', borderWidth:1,borderColor:'lightgrey'}}>
                        <Text style={{alignSelf:'center',fontSize:20,color:'black', fontWeight:'bold'}}>Sign Up</Text>
                    </Pressable>
                    <Pressable style={{height:50,width:150,borderRadius:5,backgroundColor:'#0072BB',justifyContent:'center'}} onPress={changeLogin}>
                        <Text style={{alignSelf:'center',fontSize:20,color:'white', fontWeight:'bold'}}>Sign In</Text>
                    </Pressable>
                </View>
                
            </View>

        </View>
    )
}

export default openingpage