import React, {useContext} from 'react'
import { View, Text, Pressable, StyleSheet, useWindowDimensions, Image, TextInput } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { LoginContext } from '../context/logincontext';

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
        flexDirection:'column',
    },
})

const menu = ({route, navigation}) => {
    const {token} = route.params;
    console.log(token)
    useContext(LoginContext)
    const windowHeight = useWindowDimensions().height
    const windowWidth = useWindowDimensions().width

   
    return (
        <View style={{height:windowHeight}}>
            <View style={styles.container}>
                <Pressable style={{width:40, height:40, borderRadius:20, backgroundColor:'lightgrey', margin:5,position:'absolute', right:60}}>
                    <Ionicons style={{fontSize:30,color:'black', alignSelf:'center', marginTop:5}} name='notifications-sharp'></Ionicons>
                </Pressable>

                <Pressable style={{width:40, height:40, borderRadius:20, backgroundColor:'#0072BB', margin:5,position:'absolute', right:10}}>
                    <Image style={{height:30, width:30,alignSelf:'center', marginTop:5}} source={require('../image/person.jpg')}/>
                </Pressable>
                

                <View style={{height:250, margin:20,marginTop:50}}>
                    <View style={{flexDirection:'column'}}>

                        <View style={{height:50,
                                width:375,
                                borderRadius:30,
                                borderWidth:1,
                                marginTop:10,
                                borderColor:'lightgrey',
                                color:'black',
                                flexDirection:'row'}}>
                            <TextInput style={{marginLeft:50, color:'black'}} placeholder="Search your job" placeholderTextColor="grey"/>
                            <Ionicons style={{fontSize:30,position:'absolute', left:10,  color:'black', margin:10}}name="search-sharp"></Ionicons>
                        </View>
                        
                        <Text style={{fontSize:30, margin:10,color:'black',fontWeight:'bold'}}>Welcome to Kerja 365</Text>
                        <Text style={{fontSize:15, marginLeft:10, color:'lightgrey', color:'black'}}>Arrange all your need here</Text>
                        
                        <View style={{height:70, width:375, borderRadius:30, marginTop:20, flexDirection:'row',justifyContent:"space-between", backgroundColor:'aliceblue'}}>
                            <View style={{flexDirection:'row', margin:10}}>
                                <Ionicons style={{fontSize:30, color:'black', marginTop:10}} name="albums-outline"></Ionicons>
                                <Text style={{color:'black', margin:10, marginTop:20}}>ePay Slip</Text>
                            </View>
                            
                            <View style={{flexDirection:'row', margin:10}}>
                                <Ionicons style={{fontSize:30, color:'black', marginTop:10}} name="wallet-outline"></Ionicons>
                                <Text style={{color:'black', margin:10, marginTop:20}}>BPJS</Text>
                            </View>
                            <View style={{flexDirection:'row', margin:10}}>
                                <Ionicons style={{fontSize:30, color:'black', marginTop:10}} name="cash-outline"></Ionicons>
                                <Text style={{color:'black', margin:10, marginTop:20}}>Loan</Text>
                            </View>
                        </View>
                    </View>
                </View>

                <Text style={{fontSize:30, marginLeft:30,color:'black',fontWeight:'bold'}}>Service By Catagory</Text>
                <Text style={{fontSize:15, marginLeft:30, color:'lightgrey', color:'black'}}>Find what you need</Text>
                
                <View style={{height:200, width:375, borderRadius:10,flexDirection:'column', margin:20,borderWidth:1,borderColor:'lightgrey' }}>
                    <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                        <View style={{height:75, width:170, borderRadius:10,alignItems:'center', flexDirection:'row', margin:10, backgroundColor:'aliceblue'}}>
                                <View style={{height:50, width:50, margin:10, borderRadius:25, backgroundColor:'#0072BB'}}>
                                    <Image style={{height:30, width:30, margin:10}} source={require('../image/applicant.png')}/>
                                </View>
                            <Text style={{color:'black', margin:10}}>Applycant</Text>
                        </View>
                                
                        <View style={{height:75, width:150, borderRadius:10,alignItems:'center',flexDirection:'row', margin:10, backgroundColor:'aliceblue'}}>
                            <View style={{height:50, width:50, borderRadius:25, backgroundColor:'#0072BB'}}>
                                <Image style={{height:25, width:25, margin:10}} source={require('../image/bookmark.png')}/>
                            </View>
                            <Text style={{color:'black', margin:10}}>Bookmark</Text>
                        </View>
                    </View>

                    <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                        <View style={{height:75, width:170, borderRadius:10, alignItems:'center', flexDirection:'row', margin:10, backgroundColor:'aliceblue'}}>
                                <View style={{height:50, width:50, margin:10, borderRadius:25, backgroundColor:'#0072BB'}}>
                                    <Image style={{height:30, width:30, margin:10}} source={require('../image/job-posting.png')}/>
                                </View>
                            <Text style={{color:'black', margin:10}}>Job Posting</Text>
                        </View>
                                
                        <View style={{height:75, width:150, borderRadius:10,alignItems:'center',flexDirection:'row', margin:10, backgroundColor:'aliceblue'}}>
                            <View style={{height:50, width:50, borderRadius:25, backgroundColor:'#0072BB'}}>
                                <Image style={{height:30, width:30, margin:10}} source={require('../image/your-project.png')}/>
                            </View>
                            <Text style={{color:'black', margin:10}}>Your Project</Text>
                        </View>
                    </View>
                </View>
                
            </View>

        </View>
    )
}

export default menu