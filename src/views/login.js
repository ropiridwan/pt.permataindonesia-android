import React, {useContext, useState}from 'react'
import { View, Text, Pressable, StyleSheet, useWindowDimensions, TextInput, Image } from 'react-native'
import Axios from '../axios/config'
import { LoginContext } from '../context/LoginContext';

const baseURL = "login"
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
        flexDirection:'column',
    },
})

const login = ({navigation}) => {
    const windowHeight = useWindowDimensions().height
    const windowWidth = useWindowDimensions().width

    const {setIsLogin, saveToken} = useContext(LoginContext)
    const [userName, setUsername] = useState("")
    const [passworD, setPassword] = useState("")

    const storeDataLogin = async (value) =>{
        try{
            await AsyncStorage.setItem('@token', value)

            saveToken(value)
            setIsLogin(true)
        }catch(e){
            // saving error
        }
    }

    const postLogin = () =>{
        try{
            Axios.post(baseURL,  {
                username: "johndoe",
                password: "password"
            })
              .then((response) => {
                if(response.status == 200) {
                    storeDataLogin(response.data)
                    navigation.navigate("menu",
                    {
                        token:response.data
                    })
                }
                console.log(response.data)
              })
              .catch((err)=>{
                    console.log(err)
                })
        }catch (error){
            alert(error.message)
        }
    }
    
    const changeUsername = (text)=>{
        if(text.indexOf(" ")<0){ //-1
            setUsername(text)
        }
    }

    const changePassword = (text)=>{
        setPassword(text)
    }

    return (
        <View style={{height:windowHeight}}>
            <View style={styles.container}>
                <Pressable style={{width:40, height:40, borderRadius:20, backgroundColor:'#0072BB', margin:5,position:'absolute', right:10}}>
                    <Text style={{alignSelf:'center', padding:10, color:'white'}}>EN</Text>
                </Pressable>

                <View style={{height:400, margin:20,marginTop:50}}>
                    <View style={{flexDirection:'column'}}>
                        <Text style={{fontSize:30, margin:10,color:'black',fontWeight:'bold'}}>Welcome</Text>
                        <Text style={{fontSize:15,marginLeft:10, color:'lightgrey'}}>Sign to continue and manage all your need</Text>

                        <Text style={{fontSize:20, margin:10,color:'black'}}>Username</Text>
                        <Pressable style={{height:40,width:375,borderRadius:5,backgroundColor:'lightgrey',justifyContent:'center'}}>
                            <TextInput style={{marginLeft:10, color:'black'}} value={userName}placeholder="Enter your username" placeholderTextColor='grey' onChangeText={changeUsername}/>
                        </Pressable>

                        <Text style={{fontSize:20, margin:10,color:'black'}}>Password</Text>
                        <Pressable style={{height:40,width:375,borderRadius:5,backgroundColor:'lightgrey',justifyContent:'center'}}>
                            <TextInput style={{marginLeft:10, color:'black'}}inputValue={passworD} onChangeValue={changePassword} placeholder="Enter your password" placeholderTextColor='grey'secureTextEntry={true}/>
                        </Pressable>

                        <Text style={{margin:10, color:'lightblue'}}>Forgot Passoword ?</Text>

                        <Pressable style={{height:50,width:375,borderRadius:5,backgroundColor:'#0072BB',justifyContent:'center', marginTop:20}} onPress={postLogin}>
                            <Text style={{fontSize:15,marginLeft:10, alignSelf:'center', color:'white'}}>Log in</Text>
                        </Pressable>
                    </View>
                    
                </View>

                <Text style={{fontSize:20,margin:20, color:'black',textAlign:'center'}}>Continue With</Text>
                
                <View style={{flexDirection:'row', justifyContent:"space-between", margin:50, bottom:30}}>
                    <Pressable style={{height:60,width:70, borderRadius:5,backgroundColor:'white',justifyContent:'center', borderWidth:1,borderColor:'lightgrey'}}>
                        <Image style={{height:30, width:30, margin:10, alignSelf:'center'}} source={require('../image/google.png')}/>
                    </Pressable>
                    <Pressable style={{height:60,width:70, borderRadius:5,backgroundColor:'white',justifyContent:'center', borderWidth:1,borderColor:'lightgrey'}}>
                        <Image style={{height:30, width:30, margin:10, alignSelf:'center'}} source={require('../image/fb.png')}/>
                    </Pressable>
                    <Pressable style={{height:60,width:70, borderRadius:5,backgroundColor:'white',justifyContent:'center', borderWidth:1,borderColor:'lightgrey'}}>
                        <Image style={{height:30, width:30, margin:10, alignSelf:'center'}} source={require('../image/apple.png')}/>
                    </Pressable>
                </View>
                
            </View>

        </View>
    )
}

export default login