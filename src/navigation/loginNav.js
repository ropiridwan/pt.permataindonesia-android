import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import menu from '../views/menu';

const Stack = createNativeStackNavigator();

const navigationLogin = () => {
  const [isLogin, setIsLogin] = React.useState(false)
  return (
        <Stack.Navigator>
          {
            isLogin == false ?
            <Stack.Screen name='navigationLogin' component={navigationLogin} options={{headerShown:false}} />
            :
            <>
            <Stack.Screen name='menu' component={menu} options={{headerShown:false}}/>
            </>
          }
        </Stack.Navigator>

  );
};

export default navigationLogin;
