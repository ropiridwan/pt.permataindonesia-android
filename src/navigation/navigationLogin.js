import React, {useContext, useEffect} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import openingpage from '../views/openingpage';
import login from '../views/login';
import loginNav from './loginNav'
import menu from '../views/menu';
import { LoginContext } from '../context/LoginContext';
import AsyncStorage from '@react-native-async-storage/async-storage'

const Stack = createNativeStackNavigator();

const navigationLogin = () => {
  const {isLogin, setIsLogin, saveToken} = useContext(LoginContext)

  useEffect(() => {
   getData()
 }, [])
 
  const getData= async () => {
   try{
     const tokenDatabase = await AsyncStorage.getItem('@token')
     if(tokenDatabase !== null){
       // value previously stored
       saveToken(tokenDatabase)
       setIsLogin(true)
     }
   }catch(e){
     // error reading value
   }
  }
  return (
        <Stack.Navigator>
          {
              isLogin == true ?
            <Stack.Screen name='loginNav' component={loginNav} options={{headerShown:false}}/>
              :
              <>
            <Stack.Screen name='openingpage' component={openingpage} options={{headerShown:false}}/>
            <Stack.Screen name='login' component={login} options={{headerShown:false}}/>
            <Stack.Screen name='menu' component={menu} options={{headerShown:false}}/>
              </>
          }
        </Stack.Navigator>
  );
};

export default navigationLogin;
