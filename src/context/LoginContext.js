import React from 'react'
import Axios from '../axios/config'

export const LoginContext = React.createContext()

const LoginContextProvider = (props) => {
    const [isLogin, setIsLogin] = React.useState(false)
    const [token, setToken] = React.useState('')

  const saveToken = (tokenLogin) => {
    Axios.defaults.headers.common['Authorization'] = `Bearer ${tokenLogin}`
    setToken(tokenLogin)
  }
    return (
      <LoginContext.Provider value={{isLogin, setIsLogin, token, saveToken}}>
          {props.children}
      </LoginContext.Provider>
    )
}


export default LoginContextProvider
