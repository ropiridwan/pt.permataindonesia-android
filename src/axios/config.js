import Axios from 'axios'

const instance = Axios.create({
    baseURL:"https://tasklogin.herokuapp.com/api/",
    timeout:6000,
    headers:{
        'type-Project':'mobilebanking',
        // 'Content-Type': 'application/json'
    }
})

export default instance