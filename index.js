/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import openingpage from './src/views/openingpage';
import login from './src/views/login';
import menu from './src/views/menu'
import navigationLogin from './src/navigation/navigationLogin';
import Root from './src/Root';

AppRegistry.registerComponent(appName, () => Root);
